package com.xxx.aop;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * desc:
 *
 * @author wangchengwang
 * @date 2018/10/12
 */
public class TaskMonitorAopResponse {
    private Boolean isSuccess = true;
    private String taskDesc;
    private String failureCause;
    private String partnerId;

    public Boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(Boolean success) {
        isSuccess = success;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public String getFailureCause() {
        return failureCause;
    }

    public void setFailureCause(String failureCause) {
        this.failureCause = failureCause;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
